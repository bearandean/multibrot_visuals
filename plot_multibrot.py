###
### The Mandlebrot set is the set of complex numbers 'c' such that
###   f_c(z) = z^2 + c does not diverge when iterated from 0
###  i.e. the sequence [f_c(0), f_c(f_c(0)), ...] remain bounded in L1

# modules
import numpy as np
import matplotlib.pyplot as plt
import argparse
from matplotlib.patches import Rectangle

# subclass Rectangle so that it can be called with an Axes
# instance, causing the rectangle to update its shape to match the
# bounds of the Axes
class UpdatingRect(Rectangle):
    def __call__(self, ax):
        self.set_bounds(*ax.viewLim.bounds) # the single star * unpacks the sequence/collection into positional arguments 
        ax.figure.canvas.draw_idle()

# a class that will regenerate a fractal set as we zoom in, so that you
# can actually see the increasing detail.  A box in the left panel will show
# the area to which we are zoomed.
class MandelbrotDisplay(object):
    #
    def __init__(self, h=500, w=500, niter=50, radius=2., power=2):
        self.height = h
        self.width = w
        self.niter = niter
        self.radius = radius
        self.power = power

    def __call__(self, xstart, xend, ystart, yend):
        # create the 2D grid of values for display
        self.x = np.linspace(xstart, xend, self.width)
        self.y = np.linspace(ystart, yend, self.height).reshape(-1, 1)
        threshold_time = mandelbrot(self.x, self.y, self.width, self.height, self.power, self.radius, self.niter)
        return threshold_time

    def ax_update(self, ax):
        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        # Get the number of points from the number of pixels in the window
        dims = ax.patch.get_window_extent().bounds
        self.width = int(dims[2] + 0.5)
        self.height = int(dims[2] + 0.5)

        # Get the range for the new area
        xstart, ystart, xdelta, ydelta = ax.viewLim.bounds
        xend = xstart + xdelta
        yend = ystart + ydelta

        # Update the image object with our new data and extent
        im = ax.images[-1]
        im.set_data(self.__call__(xstart, xend, ystart, yend))
        im.set_extent((xstart, xend, ystart, yend))
        ax.figure.canvas.draw_idle()

def mandelbrot(x, y, width, height, power, radius, niter):
    """returns the function applied to a specified subset of R2"""
    c = x + 1.0j * y # make the complex number
    threshold_time = np.zeros((height, width))
    z = np.zeros(threshold_time.shape, dtype = complex)
    mask = np.ones(threshold_time.shape, dtype = bool)
    for i in range(niter):
        z[mask] = z[mask]**power + c[mask]
        mask = (np.abs(z) < radius)
        threshold_time += mask
    return threshold_time


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "shows mandelbrot set display")
    # domain parameters
    parser.add_argument("--xstart", nargs = "?", type = float, default = -2.)
    parser.add_argument("--xend", nargs = "?", type = float, default = .5)
    parser.add_argument("--ystart", nargs = "?", type = float, default = -1.25)
    parser.add_argument("--yend", nargs = "?", type = float, default = 1.25)
    parser.add_argument("--width", nargs = "?", type = float, default = 500)
    parser.add_argument("--height", nargs = "?", type = float, default = 500)
    # function parameters
    parser.add_argument("--power", nargs = "?", type = int, default = 2)
    parser.add_argument("--radius", nargs = "?", type = float, default = 2.)
    parser.add_argument("--niter", nargs = "?", help = "number of iterations", type = int, default = 50)
    parser.add_argument("--color_map", nargs = "?", help = "number of iterations", type = str, default = "inferno")
    # parse
    args = parser.parse_args()
    # set initial parameters
    xstart = args.xstart
    xend = args.xend
    ystart = args.ystart
    yend = args.yend
    radius = args.radius
    width = args.width
    height = args.height
    power = args.power
    niter = args.niter
    color_map = args.color_map
    # init display class
    md = MandelbrotDisplay()
    md.__init__(h=height, w=width, niter=niter, radius=radius, power=power) # over-ride defaults with cml args
    Z = md(xstart, xend, ystart, yend) # init set domain
    # plot
    fig1, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(Z, origin='lower', cmap = color_map, extent=(md.x.min(), md.x.max(), md.y.min(), md.y.max()))
    ax2.imshow(Z, origin='lower', cmap = color_map, extent=(md.x.min(), md.x.max(), md.y.min(), md.y.max()))
    # zoom controls and updates
    rect = UpdatingRect([0, 0], 0, 0, facecolor='None', edgecolor='black', linewidth=1.0)
    rect.set_bounds(*ax2.viewLim.bounds)
    ax1.add_patch(rect)
    # Connect for changing the view limits
    ax2.callbacks.connect('xlim_changed', rect)
    ax2.callbacks.connect('ylim_changed', rect)
    #
    ax2.callbacks.connect('xlim_changed', md.ax_update)
    ax2.callbacks.connect('ylim_changed', md.ax_update)
    ax2.set_title("Zoom here")
    #
    plt.show()
