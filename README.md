# README #
multibrot_visuals.py

A python excersice to visualise [multibrot sets](https://en.wikipedia.org/wiki/Multibrot_set)
based heavily in the [matplotlib example](https://matplotlib.org/3.1.0/gallery/event_handling/viewlims.html#sphx-glr-gallery-event-handling-viewlims-py)  
	
### Set up ###
* dependencies:
	- cycler==0.10.0
	- kiwisolver==1.1.0
	- matplotlib==3.0.3
	- numpy==1.18.1
	- pkg-resources==0.0.0
	- pyparsing==2.4.6
	- python-dateutil==2.8.1
	- six==1.14.0
* suggested: create a python environment and install reqirements.txt
* usage example: 

```	
	$python complex_numbers_animation_example.py --xstart -2.0 --xend 1.0 --power 4 --radius 6 --niter 100 --color_map brg
```

### credits ###

* Andres M